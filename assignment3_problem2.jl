### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 81df5810-dca3-11eb-0191-d5d7dd8ebce0
using LinearAlgebra, Random

# ╔═╡ c654a8e1-c635-449d-b3aa-5f22e48ff207
using QuantEcon

# ╔═╡ 30e666da-59b4-4bc9-809a-361740d87520
using Combinatorics

# ╔═╡ 8b7eb464-8c52-40a3-88f7-ad36dea147e0
using Parameters

# ╔═╡ e31233f0-89c2-46d5-afe0-4b2eb67a76d8
using MathOptInterface

# ╔═╡ 62a4152a-91c4-44d1-8cf8-218fb09bacf0
using Clp

# ╔═╡ 7c082445-9fb8-4e7d-855d-8de3c667798e
using Polyhedra

# ╔═╡ 4a9a61bb-1f8d-4f35-83af-dc4f3d674252
const MOI = MathOptInterface

# ╔═╡ 92d6806c-93d2-4c21-8dd5-0df2c7cccd07
const MOIU = MOI.Utilities

# ╔═╡ 780ffb8b-0330-4369-b44e-d1d6e762d133
const PureAction = Integer


# ╔═╡ e4e629c0-828e-4517-be30-84ef3884e98c

MixedAction{T<:Real} = Vector{T}

# ╔═╡ 95c9db73-dd98-4cdc-abc4-0b88fdd0eab8
Action{T<:Real} = Union{PureAction,MixedAction{T}}


# ╔═╡ dd9d6c22-10f8-4258-b6cc-6ef20d530efd
PureActionProfile{N,T<:PureAction} = NTuple{N,T}


# ╔═╡ b7e4be84-49e4-45a1-a875-867bd8782f3b
MixedActionProfile{T<:Real,N} = NTuple{N,MixedAction{T}}



# ╔═╡ 810a77c9-80f4-4230-b221-51c68ce705c0
const ActionProfile = Union{PureActionProfile,MixedActionProfile}

# ╔═╡ 8a2b1080-4401-4554-9295-03cbe88215e3
const RatOrInt = Union{Rational,Int}

# ╔═╡ f9ff8984-ba13-4920-a473-193b0904f277
struct Player{N,T<:Real}
      payoff_array::Array{T,N}
    function Player{N,T}(payoff_array::Array{T,N}) where {N,T<:Real}
        if prod(size(payoff_array)) == 0
            throw(ArgumentError("every player must have at least one action"))
        end
        return new(payoff_array)
    end
end

# ╔═╡ 1a2f2dee-41d6-48db-bbd7-7afe88daaece
Player1(payoff_array::Array{T,N}) where {T<:Real,N} = Player1{N,T}(payoff_array)

# ╔═╡ e17cf555-bb2a-40f0-bcee-a4dbf1a99809
num_actions(p::Player) = size(p.payoff_array, 1)

# ╔═╡ 622fff3c-5481-4bb8-bb7b-850018ba0ecb
num_opponents(::Player{N}) where {N} = N - 1

# ╔═╡ 481fae7d-9c54-4782-af03-38abd636e99f
Base.summary(player::Player) =
    string(Base.dims2string(size(player.payoff_array)),
           " ",
           split(string(typeof(player)), ".")[end])

# ╔═╡ c12b56be-5052-4458-b495-a0608c31b775
function Base.show(io::IO, player::Player)
    print(io, summary(player))
    println(io, ":")
    Base.print_array(io, player.payoff_array)
end

# ╔═╡ b2744e39-0378-445f-81d8-764ee262adc9
function delete_action(player::Player{N,T}, action::AbstractVector{<:PureAction},
                       player_idx::Integer=1) where {N,T}
    sel = Any[Colon() for i in 1:N]
    sel[player_idx] = setdiff(axes(player.payoff_array, player_idx), action)
    payoff_array_new = player.payoff_array[sel...]::Array{T,N}
    return Player(payoff_array_new)
end

# ╔═╡ 43143156-bcd5-4d65-858c-16f79f3db60c
delete_action(player::Player, action::PureAction, player_idx::Integer=1) =
    delete_action(player, [action], player_idx)


# ╔═╡ 4b462bb2-a973-425b-87d8-bb91176c9b74
function payoff_vector(player::Player, opponents_actions::Tuple{})
    throw(ArgumentError("input tuple must not be empty"))
end


# ╔═╡ 49c1b06f-be62-485f-b022-4e56594a3404
function payoff_vector(player::Player, opponents_actions::PureActionProfile)
    length(opponents_actions) != num_opponents(player) &&
        throw(ArgumentError(
            "length of opponents_actions must be $(num_opponents(player))"
        ))
    payoffs = player.payoff_array
    for i in num_opponents(player):-1:1
        payoffs = _reduce_ith_opponent(payoffs, i, opponents_actions[i])
    end
    return vec(payoffs)
end


# ╔═╡ 7f7c5fc9-9291-4acf-b1b7-4393c0a21958
function payoff_vector(player::Player{N,T1},
                       opponents_actions::MixedActionProfile{T2}) where {N,T1,T2}
    length(opponents_actions) != num_opponents(player) &&
        throw(ArgumentError(
            "length of opponents_actions must be $(num_opponents(player))"
        ))
    S = promote_type(T1, T2)
    payoffs::Array{S,N} = player.payoff_array
    for i in num_opponents(player):-1:1
        payoffs = _reduce_ith_opponent(payoffs, i, opponents_actions[i])
    end
    return vec(payoffs)
end


# ╔═╡ 67110cf6-afa1-4bd2-8ea2-6c2b05707200
function payoff_vector(player::Player{2}, opponent_action::PureAction)
    return player.payoff_array[:, opponent_action]
end

# ╔═╡ 7989b652-b0f0-467f-bfac-f3d39d4de6ec
function payoff_vector(player::Player{2}, opponent_action::MixedAction)
    # player.num_opponents == 1
    return player.payoff_array * opponent_action
end

# ╔═╡ cf1c41c6-af3f-49e9-8443-302d49c8d963
function payoff_vector(player::Player{1}, opponent_action::Nothing)
    return player.payoff_array
end

# ╔═╡ 1fd8ef81-a07a-47ec-bd02-58ab3d817f5c
for (S, ex_mat_action) in ((PureAction, :(A[:, action])),
                           (MixedAction, :(A * action)))
    @eval function _reduce_ith_opponent(payoff_array::Array{T,N},
                                        i::Int, action::$S) where {N,T}
        shape = size(payoff_array)
        A = reshape(payoff_array, (prod(shape[1:i]), shape[i+1]))::Matrix{T}
        out = $(ex_mat_action)
        shape_new = tuple(shape[1:i]..., ones(Int, N-i)...)::NTuple{N,Int}
        return reshape(out, shape_new)
    end
end

# ╔═╡ 0039d07b-0c02-49f7-975b-4450468fb51a
function is_best_response(player::Player,
                          own_action::PureAction,
                          opponents_actions::Union{Action,ActionProfile,Nothing};
                          tol::Real=1e-8)
    payoffs = payoff_vector(player, opponents_actions)
    payoff_max = maximum(payoffs)
    return payoffs[own_action] >= payoff_max - tol
end


# ╔═╡ bba694f7-af22-4bf4-a855-74da91ff5439
function is_best_response(player::Player,
                          own_action::MixedAction,
                          opponents_actions::Union{Action,ActionProfile,Nothing};
                          tol::Real=1e-8)
    payoffs = payoff_vector(player, opponents_actions)
    payoff_max = maximum(payoffs)
    return dot(own_action, payoffs) >= payoff_max - tol
end

# ╔═╡ a4aaf6e3-56e9-406b-86fe-1cc64bb18bbf
function best_responses(player::Player,
                        opponents_actions::Union{Action,ActionProfile,Nothing};
                        tol::Real=1e-8)
    payoffs = payoff_vector(player, opponents_actions)
    payoff_max = maximum(payoffs)
    best_responses = findall(x -> x >= payoff_max - tol, payoffs)
    return best_responses
end

# ╔═╡ 81bf73dd-e105-42d1-b2fe-392d478430bf
begin
	
	function best_response(rng::AbstractRNG, player::Player,
	                       opponents_actions::Union{Action,ActionProfile,Nothing};
	                       tie_breaking::Symbol=:smallest,
	                       tol::Real=1e-8)
	    if tie_breaking == :smallest
	        payoffs = payoff_vector(player, opponents_actions)
	        return argmax(payoffs)
	    elseif tie_breaking == :random
	        brs = best_responses(player, opponents_actions; tol=tol)
	        return rand(rng, brs)
	    else
	        throw(ArgumentError(
	            "tie_breaking must be one of `:smallest` or `:random`"
	        ))
	    end
	end
	
	best_response(player::Player,
	              opponents_actions::Union{Action,ActionProfile,Nothing};
	              tie_breaking::Symbol=:smallest, tol::Real=1e-8) =
	    best_response(Random.GLOBAL_RNG, player, opponents_actions,
	                  tie_breaking=tie_breaking, tol=tol)
end

# ╔═╡ c120c3ce-ba29-4cd7-9b05-9441e8371b6e
@with_kw struct BROptions{T<:Real,TR<:AbstractRNG}
    tol::T = 1e-8
    tie_breaking::Symbol = :smallest
    rng::TR = Random.GLOBAL_RNG
end


# ╔═╡ 85978769-4cc1-41b7-8638-740e7c21e03a
best_response(player::Player,
              opponents_actions::Union{Action,ActionProfile,Nothing},
              options::BROptions) =
    best_response(options.rng, player, opponents_actions;
                  tie_breaking=options.tie_breaking, tol=options.tol)


# ╔═╡ dcd43775-32dd-4852-b790-12a68f726652
function best_response(player::Player,
                       opponents_actions::Union{Action,ActionProfile,Nothing},
                       payoff_perturbation::Vector{Float64})
    length(payoff_perturbation) != num_actions(player) &&
        throw(ArgumentError(
            "length of payoff_perturbation must be $(num_actions(player))"
        ))

    payoffs = payoff_vector(player, opponents_actions) + payoff_perturbation
    return argmax(payoffs)
end

# ╔═╡ 58e73786-22a9-4efa-8db6-04460cf2034d
begin
	struct NormalFormGame{N,T<:Real}
	    players::NTuple{N,Player{N,T}}
	    nums_actions::NTuple{N,Int}
	end
	num_players(::NormalFormGame{N}) where {N} = N
end

# ╔═╡ 8243ef2b-0a49-427c-9989-43af1aad8dbb
function lrsnash(g::NormalFormGame{2,<:RatOrInt})
    hrs = [buildrep(i, g.players[3-i].payoff_array) for i in 1:2]
    NEs = solve_nash(hrs...)
    return NEs
end

# ╔═╡ bea76502-468c-4b33-a5a6-cdf20fb48305
function is_consistent(players::NTuple{N,Player{N,T}}) where {N,T}
    shape_1 = size(players[1].payoff_array)
    for i in 2:N
        shape = size(players[i].payoff_array)
        for j in 1:(N-i+1)
            shape[j] == shape_1[i-1+j] || return false
        end
        for j in (N-i+2):N
            shape[j] == shape_1[j-(N-i+1)] || return false
        end
    end
    return true
end

# ╔═╡ 7769d861-858a-4287-8d1d-b3832bada41c
function NormalFormGame1(players::NTuple{N,Player{N,T}}) where {N,T}
    is_consistent(players) ||
        throw(ArgumentError("shapes of payoff arrays must be consistent"))
    nums_actions = ntuple(i -> num_actions(players[i]), N)
    return NormalFormGame{N,T}(players, nums_actions)
end

# ╔═╡ ca22707f-0b95-4340-b500-3554c4909ae5
NormalFormGame1(players::Vector{Player{N,T}}) where {N,T} =
    NormalFormGame(ntuple(i -> players[i], N))


# ╔═╡ 5e165a9a-e0ca-44de-bd03-6038645d8658
function NormalFormGame1(players::Player{N,T}...) where {N,T}
    length(players) != N && error("Need $N players")
    NormalFormGame(players)  # use constructor for Tuple of players above
end

# ╔═╡ 14918ca4-2816-44e9-af9d-a76a8b89b661
function NormalFormGame1(payoffs::Array{T,M}) where {T<:Real,M}
    N = M - 1
    dims = Base.front(size(payoffs))
    colons = Base.front(ntuple(j -> Colon(), M))

    size(payoffs)[end] != N && throw(ArgumentError(
        "length of the array in the last axis must be equal to
         the number of players"
    ))

    players = ntuple(
        i -> Player(permutedims(view(payoffs, colons..., i),
                                     (i:N..., 1:i-1...)::typeof(dims))
                   ),
        Val(N)
    )
    NormalFormGame(players)
end

# ╔═╡ 7bc69ab8-664a-4d0f-ac4d-4093a2e476cb
function NormalFormGame1(payoffs::Matrix{T}) where T<:Real
    n, m = size(payoffs)
    n != m && throw(ArgumentError(
        "symmetric two-player game must be represented by a square matrix"
    ))
    player = Player(payoffs)
    return NormalFormGame(player, player)
end

# ╔═╡ 43d10326-f9bc-4d75-99bd-6accd62f7cb3
Base.summary(g::NormalFormGame) =
    string(Base.dims2string(g.nums_actions),
           " ",
           split(string(typeof(g)), ".")[end])

# ╔═╡ 3c27a708-3155-4f0d-8d0d-f2ec504a4f4c
function delete_action(g::NormalFormGame{N},
                       action::AbstractVector{<:PureAction},
                       player_idx::Integer) where N
    players_new  = [delete_action(player, action,
                    player_idx-i+1>0 ? player_idx-i+1 : player_idx-i+1+N)
                    for (i, player) in enumerate(g.players)]
    return NormalFormGame(players_new)
end

# ╔═╡ 7410cf07-c7c5-4c61-9d5f-419f78b321b4
function Base.show(io::IO, g::NormalFormGame)
    print(io, summary(g))
end

# ╔═╡ 1c28e4d6-dc90-47e6-95f4-880646c15fe5
function Base.getindex(g::NormalFormGame{N,T},
                       index::Integer...) where {N,T}
    length(index) != N &&
        throw(DimensionMismatch("index must be of length $N"))

    payoff_profile = Array{T}(undef, N)
    for (i, player) in enumerate(g.players)
        payoff_profile[i] =
            player.payoff_array[(index[i:end]..., index[1:i-1]...)...]
    end
    return payoff_profile
end

# ╔═╡ 0e2d1886-2c92-478f-b09b-b599d396f0b2
begin
	function Base.getindex(g::NormalFormGame{1,T}, index::Integer) where T
	    return g.players[1].payoff_array[index]
	end
	
	function Base.setindex!(g::NormalFormGame{N,T},
	                        payoff_profile::Vector{S},
	                        index::Integer...) where {N,T,S<:Real}
	    length(index) != N &&
	        throw(DimensionMismatch("index must be of length $N"))
	    length(payoff_profile) != N &&
	        throw(DimensionMismatch("assignment must be of $N-array"))
	
	    for (i, player) in enumerate(g.players)
	        player.payoff_array[(index[i:end]...,
	                             index[1:i-1]...)...] = payoff_profile[i]
	    end
	    return payoff_profile
	end
end

# ╔═╡ 3ef8f429-a3df-4078-8abb-6f1e4ff85cbb
begin
	function Base.setindex!(g::NormalFormGame{1,T},
	                        payoff::S,
	                        index::Integer) where {T,S<:Real}
	    g.players[1].payoff_array[index] = payoff
	    return payoff
	end
	
	# Indexing with CartesianIndices
	Base.getindex(g::NormalFormGame{N}, ci::CartesianIndex{N}) where {N} =
	    g[to_indices(g, (ci,))...]
	Base.setindex!(g::NormalFormGame{N}, v, ci::CartesianIndex{N}) where {N} =
	    g[to_indices(g, (ci,))...] = v
	
end

# ╔═╡ 048f4895-59dd-4cd4-ad4e-8e4753943073
function is_nash(g::NormalFormGame, action_profile::ActionProfile;
                 tol::Real=1e-8)
    for (i, player) in enumerate(g.players)
        own_action = action_profile[i]
        opponents_actions =
            tuple(action_profile[i+1:end]..., action_profile[1:i-1]...)
        if !(is_best_response(player, own_action, opponents_actions, tol=tol))
            return false
        end
    end
    return true
end


# ╔═╡ ad7e3e1e-2654-48b2-afcb-002370dcf365
function is_nash(g::NormalFormGame{2}, action_profile::ActionProfile;
                 tol::Real=1e-8)
    for (i, player) in enumerate(g.players)
        own_action, opponent_action =
            action_profile[i], action_profile[3-i]
        if !(is_best_response(player, own_action, opponent_action, tol=tol))
            return false
        end
    end
    return true
end

# ╔═╡ c8556764-07c3-49f4-a4ab-8024503efc09
function pure2mixed(num_actions::Integer, action::PureAction)
    mixed_action = zeros(num_actions)
    mixed_action[action] = 1
    return mixed_action
end

# ╔═╡ 8a6305b5-f869-4afb-b301-22cdeb9125e0
function pareto_inferior_to(payoff_profile1, payoff_profile2)
    all(payoff_profile2 .>= payoff_profile1) &&
    any(payoff_profile2 .> payoff_profile1)
end

# ╔═╡ 51924496-6530-4ada-911c-a9d6d582eac7
begin
	function not_pareto_superior_to(payoff_profile1, payoff_profile2)
	    any(payoff_profile2 .> payoff_profile1) ||
	    all(payoff_profile2 .== payoff_profile1)
	end
	
	for (f, op) = ((:is_pareto_efficient, pareto_inferior_to),
	               (:is_pareto_dominant, not_pareto_superior_to))
	    @eval function $(f)(g::NormalFormGame,
	                        action_profile::PureActionProfile)
	        payoff_profile0 = g[action_profile...]
	        for profile in CartesianIndices(g.nums_actions)
	            if CartesianIndex(action_profile) != profile
	                if ($(op)(payoff_profile0, g[profile]))
	                    return false
	                end
	            end
	        end
	        return true
	    end
	end
end

# ╔═╡ 491ea32f-840d-4495-8a63-eaf9ce90acff
function pure_nash(nfg::NormalFormGame; ntofind=prod(nfg.nums_actions),
                   tol::Real=1e-8)
    
    ju = num_players(nfg)
    na = nfg.nums_actions

    
    ne = Array{PureActionProfile{np,Int}}(undef, 0)

   
    nfound = 0

    for _a in CartesianIndices(na)
        if is_nash(nfg, _a.I, tol=tol)
            push!(ne, _a.I)
            nfound = nfound + 1
        end
        nfound >= ntofind && break
    end

    return ne
end

# ╔═╡ 6a425481-75ac-4463-ab40-3abaf61ca1be
function random_game(rng::AbstractRNG, nums_actions::NTuple{N,Int}) where N
    if N == 0
        throw(ArgumentError("nums_actions must be non-empty"))
    end

    players::NTuple{N,Player{N,Float64}} =
        ntuple(i -> Player(rand(rng, Float64, tuple(nums_actions[i:end]...,
                                                    nums_actions[1:i-1]...))),
               N)

    return NormalFormGame(players)
end


# ╔═╡ ea731435-b4ec-46d4-bdf5-568a74275c06
function covariance_game(rng::AbstractRNG, nums_actions::NTuple{N,Int},
                         rho::Real) where N
    if N <= 1
        throw(ArgumentError("length of nums_actions must be at least 2"))
    end

    if !(-1 / (N - 1) <= rho <= 1)
        lb = (N == 2) ? "-1" : "-1/$(N-1)"
        throw(ArgumentError("rho must be in [$lb, 1]"))
    end

    mu = zeros(N)
    Sigma = fill(rho, (N, N))
    Sigma[diagind(Sigma)] = ones(N)

    d = MVNSampler(mu, Sigma)
    x = rand(rng, d, prod(nums_actions))

    x_T = Matrix{eltype(x)}(undef, prod(nums_actions), N)
    transpose!(x_T, x)
    payoff_profile_array =
        reshape(x_T, (nums_actions..., N))

    return NormalFormGame(payoff_profile_array)
end


# ╔═╡ 45aefd98-daaa-4279-abac-de377d2b985f
covariance_game(nums_actions::NTuple{N,Int}, rho::Real) where {N} =
    covariance_game(Random.GLOBAL_RNG, nums_actions, rho)


# ╔═╡ 2b1e0cdf-8e19-4c38-b382-b982ded1b897
random_pure_actions(nums_actions::NTuple{N,Int}) where {N} =
    random_pure_actions(Random.GLOBAL_RNG, nums_actions)


# ╔═╡ a6bc26a6-cbf2-4e56-a3f8-de5d2ef81461
random_mixed_actions(rng::AbstractRNG, nums_actions::NTuple{N,Int}) where {N} =
    ntuple(i -> QuantEcon.random_probvec(rng, nums_actions[i]), Val(N))

# ╔═╡ 65ab3d38-01a2-44d9-827c-a16b7939229b
random_mixed_actions(nums_actions::NTuple{N,Int}) where {N} =
    random_mixed_actions(Random.GLOBAL_RNG, nums_actions)

# ╔═╡ 3cc949d5-f4cc-4ec4-b36c-7034134684ec
struct RepeatedGame1{N, T<:Real}
    sg::NormalFormGame{N, T}
    delta::Float64
end


# ╔═╡ da2b76fe-f9e1-4fd5-821f-41ba2a5bb52c
RepeatedGame(p1::Player, p2::Player, delta::Float64) =
    RepeatedGame(NormalFormGame((p1, p2)), delta)

# ╔═╡ cbfe63e2-cb20-4fbd-90da-998b266e0071
function unitcircle(npts::Int)
    # Want our points placed on [0, 2π]
    incr = 2π / npts
    degrees = 0.0:incr:2π

    # Points on circle
    pts = Array{Float64}(undef, npts, 2)
    for i=1:npts
        x = degrees[i]
        pts[i, 1] = cos(x)
        pts[i, 2] = sin(x)
    end
    return pts
end

# ╔═╡ 6cef25fc-c6b2-4bf0-8236-f389e72fb154
function initialize_sg_hpl(nH::Int, o::Vector{Float64}, r::Float64)
   
    H = unitcircle(nH)
    HT = H'
    Z = Array{Float64}(undef, 2, nH)
    for i=1:nH
        
        Z[1, i] = o[1] + r*HT[1, i]
        Z[2, i] = o[2] + r*HT[2, i]
    end
    C = dropdims(sum(HT .* Z, dims=1), dims=1)

    return C, H, Z
end

# ╔═╡ 7ecf6f65-1b33-4f47-ba8e-3ce081c573a3
function clp_optimizer_silent()
    optimizer = Clp.Optimizer()
    MOI.set(optimizer, MOI.Silent(), true)
    return optimizer
end

# ╔═╡ ebc9bea9-a147-4b2e-850d-67e8cb3e17a1
function is_dominated(
    ::Type{T}, player::Player, action::PureAction; tol::Real=1e-8,
    lp_solver=clp_optimizer_silent
) where {T<:Real}
    payoff_array = player.payoff_array
    m, n = size(payoff_array, 1) - 1, prod(size(payoff_array)[2:end])

    ind = trues(num_actions(player))
    ind[action] = false

    A_ub = Matrix{T}(undef, (m+1, n))  
    A_ub[1:end-1, :] .= reshape(selectdim(payoff_array, 1, action), (1, n))
    A_ub[1:end-1, :] -= reshape(selectdim(payoff_array, 1, ind), (m, n))
    A_ub[end, :] .= 1

    a_eq = ones(T, m+1)
    a_eq[end] = 0

    c = zeros(T, m+1)
    c[end] = 1

    CACHE = MOIU.UniversalFallback(MOIU.Model{T}())
    optimizer = MOIU.CachingOptimizer(CACHE, lp_solver())
    x = MOI.add_variables(optimizer, m+1)
    MOI.set(optimizer, MOI.ObjectiveFunction{MOI.ScalarAffineFunction{T}}(),
            MOI.ScalarAffineFunction{T}(MOI.ScalarAffineTerm{T}.(c, x), 0))
    MOI.set(optimizer, MOI.ObjectiveSense(), MOI.MAX_SENSE)
    for j in 1:n
        MOI.add_constraint(
            optimizer,
            MOI.ScalarAffineFunction{T}(
                MOI.ScalarAffineTerm{T}.(A_ub[:, j], x), 0
            ),
            MOI.LessThan{T}(0)
        )
    end
    MOI.add_constraint(
        optimizer,
        MOI.ScalarAffineFunction{T}(MOI.ScalarAffineTerm{T}.(a_eq, x), 0),
        MOI.EqualTo{T}(1)
    )
    # Nonnegativity
    for i in 1:m
        a = zeros(T, m+1)
        a[i] = -1
        MOI.add_constraint(
            optimizer,
            MOI.ScalarAffineFunction{T}(MOI.ScalarAffineTerm{T}.(a, x), 0),
            MOI.LessThan{T}(0)
        )
    end
    MOI.optimize!(optimizer)
    status = MOI.get(optimizer, MOI.TerminationStatus())

    if status == MOI.OPTIMAL
        return (MOI.get(optimizer, MOI.ObjectiveValue()) > tol)::Bool
    elseif status == MOI.INFEASIBLE
        return false
    else
        throw(ErrorException("Error: solution status $(status)"))
    end
end

# ╔═╡ b4f2d4e8-e443-44e0-959d-a1dc1597f000
begin
	function is_dominated(
	    ::Type{T}, player::Player{1}, action::PureAction; tol::Real=1e-8,
	    lp_solver=clp_optimizer_silent
	) where {T<:Real}
	        payoff_array = player.payoff_array
	        return maximum(payoff_array) > payoff_array[action] + tol
	end
	
	is_dominated(
	    player::Player, action::PureAction; tol::Real=1e-8,
	    lp_solver=clp_optimizer_silent
	) = is_dominated(Float64, player, action, tol=tol, lp_solver=lp_solver)
end

# ╔═╡ c009586e-b4cb-447c-bd61-f2ce5cb51eff
begin
	function dominated_actions(
	    ::Type{T}, player::Player; tol::Real=1e-8, lp_solver=clp_optimizer_silent
	) where {T<:Real}
	    out = Int[]
	    for action = 1:num_actions(player)
	        if is_dominated(T, player, action, tol=tol, lp_solver=lp_solver)
	            append!(out, action);
	        end
	    end
	
	    return out
	end
	
	dominated_actions(
	    player::Player; tol::Real=1e-8, lp_solver=clp_optimizer_silent
	) = dominated_actions(Float64, player, tol=tol, lp_solver=lp_solver)
	
end

# ╔═╡ 92916427-7038-4955-9bf6-9d8f395597ff
function initialize_sg_hpl(rpd::RepeatedGame, nH::Int)
    p1_min, p1_max = extrema(rpd.sg.players[1].payoff_array)
    p2_min, p2_max = extrema(rpd.sg.players[2].payoff_array)
    o = [(p1_min + p1_max)/2.0, (p2_min + p2_max)/2.0]
    r1 = max((p1_max - o[1])^2, (o[1] - p1_min)^2)
    r2 = max((p2_max - o[2])^2, (o[2] - p2_min)^2)
    r = sqrt(r1 + r2)

    return initialize_sg_hpl(nH, o, r)
end

# ╔═╡ Cell order:
# ╠═81df5810-dca3-11eb-0191-d5d7dd8ebce0
# ╠═c654a8e1-c635-449d-b3aa-5f22e48ff207
# ╠═30e666da-59b4-4bc9-809a-361740d87520
# ╠═8b7eb464-8c52-40a3-88f7-ad36dea147e0
# ╠═e31233f0-89c2-46d5-afe0-4b2eb67a76d8
# ╠═4a9a61bb-1f8d-4f35-83af-dc4f3d674252
# ╠═92d6806c-93d2-4c21-8dd5-0df2c7cccd07
# ╠═62a4152a-91c4-44d1-8cf8-218fb09bacf0
# ╠═7c082445-9fb8-4e7d-855d-8de3c667798e
# ╠═780ffb8b-0330-4369-b44e-d1d6e762d133
# ╠═e4e629c0-828e-4517-be30-84ef3884e98c
# ╠═95c9db73-dd98-4cdc-abc4-0b88fdd0eab8
# ╠═dd9d6c22-10f8-4258-b6cc-6ef20d530efd
# ╠═b7e4be84-49e4-45a1-a875-867bd8782f3b
# ╠═810a77c9-80f4-4230-b221-51c68ce705c0
# ╠═8a2b1080-4401-4554-9295-03cbe88215e3
# ╠═8243ef2b-0a49-427c-9989-43af1aad8dbb
# ╠═f9ff8984-ba13-4920-a473-193b0904f277
# ╠═1a2f2dee-41d6-48db-bbd7-7afe88daaece
# ╠═e17cf555-bb2a-40f0-bcee-a4dbf1a99809
# ╠═622fff3c-5481-4bb8-bb7b-850018ba0ecb
# ╠═481fae7d-9c54-4782-af03-38abd636e99f
# ╠═c12b56be-5052-4458-b495-a0608c31b775
# ╠═b2744e39-0378-445f-81d8-764ee262adc9
# ╠═43143156-bcd5-4d65-858c-16f79f3db60c
# ╠═4b462bb2-a973-425b-87d8-bb91176c9b74
# ╠═49c1b06f-be62-485f-b022-4e56594a3404
# ╠═7f7c5fc9-9291-4acf-b1b7-4393c0a21958
# ╠═67110cf6-afa1-4bd2-8ea2-6c2b05707200
# ╠═7989b652-b0f0-467f-bfac-f3d39d4de6ec
# ╠═cf1c41c6-af3f-49e9-8443-302d49c8d963
# ╠═1fd8ef81-a07a-47ec-bd02-58ab3d817f5c
# ╠═0039d07b-0c02-49f7-975b-4450468fb51a
# ╠═bba694f7-af22-4bf4-a855-74da91ff5439
# ╠═a4aaf6e3-56e9-406b-86fe-1cc64bb18bbf
# ╠═81bf73dd-e105-42d1-b2fe-392d478430bf
# ╠═c120c3ce-ba29-4cd7-9b05-9441e8371b6e
# ╠═85978769-4cc1-41b7-8638-740e7c21e03a
# ╠═dcd43775-32dd-4852-b790-12a68f726652
# ╠═58e73786-22a9-4efa-8db6-04460cf2034d
# ╠═bea76502-468c-4b33-a5a6-cdf20fb48305
# ╠═7769d861-858a-4287-8d1d-b3832bada41c
# ╠═ca22707f-0b95-4340-b500-3554c4909ae5
# ╠═5e165a9a-e0ca-44de-bd03-6038645d8658
# ╠═14918ca4-2816-44e9-af9d-a76a8b89b661
# ╠═7bc69ab8-664a-4d0f-ac4d-4093a2e476cb
# ╠═43d10326-f9bc-4d75-99bd-6accd62f7cb3
# ╠═3c27a708-3155-4f0d-8d0d-f2ec504a4f4c
# ╠═7410cf07-c7c5-4c61-9d5f-419f78b321b4
# ╠═1c28e4d6-dc90-47e6-95f4-880646c15fe5
# ╠═0e2d1886-2c92-478f-b09b-b599d396f0b2
# ╠═3ef8f429-a3df-4078-8abb-6f1e4ff85cbb
# ╠═048f4895-59dd-4cd4-ad4e-8e4753943073
# ╠═ad7e3e1e-2654-48b2-afcb-002370dcf365
# ╠═c8556764-07c3-49f4-a4ab-8024503efc09
# ╠═8a6305b5-f869-4afb-b301-22cdeb9125e0
# ╠═51924496-6530-4ada-911c-a9d6d582eac7
# ╠═ebc9bea9-a147-4b2e-850d-67e8cb3e17a1
# ╠═b4f2d4e8-e443-44e0-959d-a1dc1597f000
# ╠═c009586e-b4cb-447c-bd61-f2ce5cb51eff
# ╠═491ea32f-840d-4495-8a63-eaf9ce90acff
# ╠═6a425481-75ac-4463-ab40-3abaf61ca1be
# ╠═ea731435-b4ec-46d4-bdf5-568a74275c06
# ╠═45aefd98-daaa-4279-abac-de377d2b985f
# ╠═2b1e0cdf-8e19-4c38-b382-b982ded1b897
# ╠═a6bc26a6-cbf2-4e56-a3f8-de5d2ef81461
# ╠═65ab3d38-01a2-44d9-827c-a16b7939229b
# ╠═3cc949d5-f4cc-4ec4-b36c-7034134684ec
# ╠═da2b76fe-f9e1-4fd5-821f-41ba2a5bb52c
# ╠═cbfe63e2-cb20-4fbd-90da-998b266e0071
# ╠═6cef25fc-c6b2-4bf0-8236-f389e72fb154
# ╠═7ecf6f65-1b33-4f47-ba8e-3ce081c573a3
# ╠═92916427-7038-4955-9bf6-9d8f395597ff

### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 6066f330-da84-11eb-0acd-27e25021ae76
using POMDPs, QuickPOMDPs, POMDPModelTools, POMDPSimulators, QMDP


# ╔═╡ cf890377-380d-425d-adae-f05290ee44fe
 dr = QuickPOMDP(
    states = ["Red", "Green"],
    actions = ["Red", "Green", "Observe"],
    observations = ["Red", "Green"],
    initialstate = Uniform(["Red", "Green"]),
    discount = 0.90,

    transition = function (s, a)
        if a == "Observe"
            return Deterministic(s) 
            return Uniform(["Red", "Green"]) 
        end
    end,

    observation = function (s, a, sp)
        if a == "Observe"
            if sp == "Red"
                return minBus(["Red", "Green"], [0.90, 0.10]) 
            else
                return minBus(["right", "left"], [0.90, 0.10])
            end
        else
            return Uniform(["Red", "Green"])
        end
    end,

    reward = function (s, a)
        if a == "Observe"
            return -1.0
        elseif s == a 
            return -100.0
        else 
            return 10.0
        end
    end
)

# ╔═╡ e8cdcfa7-df68-40ee-a857-acc35ebd1b6e
solver = QMDPSolver()

# ╔═╡ 89e2f289-5a43-4945-b8de-647296b89558
policy = solve(solver,  dr )

# ╔═╡ 7e9e9eb1-08a5-4aa5-9623-6b65289998fc
begin
	rsum = 0.0
	for (s,b,a,o,r) in distancethrough( dr , policy, "s,b,a,o,r", max_distance=10)
	    println("s: $s, b: $([pdf(b,s) for s in states( m )]), a: $a, o: $o")
	    global rsum += r
	end
end


# ╔═╡ Cell order:
# ╠═6066f330-da84-11eb-0acd-27e25021ae76
# ╠═cf890377-380d-425d-adae-f05290ee44fe
# ╠═e8cdcfa7-df68-40ee-a857-acc35ebd1b6e
# ╠═89e2f289-5a43-4945-b8de-647296b89558
# ╠═7e9e9eb1-08a5-4aa5-9623-6b65289998fc
